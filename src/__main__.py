#!/usr/bin/env python

# __main__.py
# Noxninstaller v.
# (C) 202307 <author>
#
# Add description here

# base imports
import sys
import importlib.resources

# local imports

# from imports
from PySide6.QtGui import QtGuiApplication
from PySide6.QtQml import QQmlApplicationEngine, QmlElement
from Pyside6.QtCore import Signal, QObject, Slot, Property

# Bridge class that connects signals and slots with the QML GUI
# NOTE: Bridge class might be removed depending on project needs
QML_IMPORT_NAME = "<add namespace here>.<add module here>"
QML_IMPORT_MAJOR_VERSION = 1


if __name__ == "__main__":
    print('Noxninstaller')
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    # to quit the app from the menu?
    engine.quit.connect(app.quit)

    qml_gui_file = importlib.resources.files("gui").joinpath("main.qml")
    qml_file = importlib.resources.as_file(qml_gui_file)

    with qml_gui_file as qfile:
        engine.load(qfile)

    bridge = Bridge()
    engine.rootObjects()[0].setProperty('bridge', bridge)
    if not engine.rootObjects():
        sys.exit(-1)

    exit_code = app.exec()
    del engine
    sys.exit(exit_code)
